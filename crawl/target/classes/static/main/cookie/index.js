$(function(){
	
	$('#cookie').panel({
	    tools:[
		    {
			    iconCls:'icon-save',
			    handler:function(){
			    	saveCookie();
			    }
		    }
		]
	});
	
	$('#btn_save').bind('click',function(){
		saveCookie();
	})
	
	//请求Cookie
	$.ajax({
        type: "GET",
        url: "/cookie/getCookie",
        dataType: "json",
        success: function(data){
        	if(data.code == 200){
        		if(data.t != null){
        			$("textarea[name='jessionId']").val(data.t.cookie[0]);
        			$("textarea[name='qquc']").val(data.t.cookie[1]);
        		}
        	}else{
        		alert(data.t.msg);
        	}
        }
    });
	
})

function saveCookie(){
	var jessionId = $("textarea[name='jessionId']").val();
	var qquc = $("textarea[name='qquc']").val();
	if(jessionId == '' || qquc==''){
		alert('请填写完整的Cookie信息!');
		return false;
	}
	$.ajax({
        type: "POST",
        url: "/cookie/setCookie",
        data: {jessionId:jessionId, qquc:qquc},
        dataType: "json",
        success: function(data){
        	if(data.code == 200){
        		alert(data.msg);
        	}else{
        		alert(data.msg);
        	}
        }
    });
}