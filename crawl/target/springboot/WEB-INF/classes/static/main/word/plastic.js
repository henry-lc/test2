$(function(){
	var localCacheUrl = "/plastic/word/getPlasticList";
	var crawlAllDataUrl = "/plastic/word/getPlasticCrawl";
	var editUrl = "/plastic/word/savePlastic";
	var exportWordUrl = "/plastic/word/exportPlastic";
	var clearCrawlUrl = "/plastic/word/clearPlasticCrawlCache";
	
	datagridObj.createDataGrid("plasticTable",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl);
	datagridObj.createLoadingMsg("plasticTable");
})