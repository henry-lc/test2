$(function(){
	var localCacheUrl = "/steel/word/getSteelPriceList";
	var crawlAllDataUrl = "/steel/word/getSteelPriceCrawl";
	var editUrl = "/steel/word/savePrice";
	var exportWordUrl = "/steel/word/exportPrice";
	var clearCrawlUrl = "/steel/word/clearPriceCrawlCache";
	
	datagridObj.createDataGrid("steelPrice",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl);
	datagridObj.createLoadingMsg("steelPrice");
})