package com.project.app.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hwpf.usermodel.Paragraph;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.app.cookie.CookieEntity;
import com.project.app.service.ExcelService;
import com.project.app.service.QlmService;
import com.project.app.service.TaoBaoService;



@RunWith(SpringRunner.class)
@SpringBootTest
public class SteelServiceImplTest {

	@Autowired
	private QlmService qlmService;

	@Autowired
	private TaoBaoService taoBaoService;

	@Autowired
	private ExcelService excelService;

	@Test
	public void test() {
		taoBaoService.saveToTarget();
	}


	@Test
	public void testHttp() {
		List<String> ls = new ArrayList<String>();
		CookieEntity cookie = CookieEntity.getInstance();
		ls.add("JSESSIONID=B72EC47339A1CC7531F1AD7808071E56; path=/; domain=.fg.f139.com; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT;");
		ls.add("_qquc=6d2af3823bb4829a95200dba06ccb0265939e590873e5cef97acea244ae82ffcbf5e2403097381fa2d9a6c4604d69e51c39870b2b267c7dc071b78fac39ed440f054a58ca304d4f0925129c937cb6935b4631b12291ad3bed71e3c7cf68174181f424d32347fd2822284431a74dec2c4cc5c933b96a2fb90f9fb875d1913e418d4067afd041d5b609710f6af04f51f6eebca780348e634956059663b1ea0c63671347d330ba71e63b4e66b9adc119ddfd2fac9e3fc13e07e531a3d579fc852778ca3001bd4cf1ae740651d83de34fc405f741b3f69aac578d05fe26961e012551b13b7a2557df4b9b04eb95c349a9c0903c9e144a1661d0f8fc78ef8b87f3bf7ae89672f4ff196aadc60a8eae6483baef32e52a595d5e141717f6417f948aaa279d001d13b7a2bee2460d1d835ce38d700864368eab8f2b10f31642b295093604226def5e00d3d6a929c2e4596344032c1820b0aef130f6ea74ed62a82d674a3662fac6eabb5f0e192f276ac62fcb3c4e84c509348fb563149d62e34c91e8db72d06d96cd592ac67487e0c7e8b88514b4f7553ffd3926c330032c8d0ffe980490845dfcbbbd0e27bd32e651ee646f14489a0477bb02adfb25059a7bac52a5d7e4f2af908edfcdc4889d29de665fb24d0bcf5161682270e5fb61e3e7913dd5d93; path=/; domain=.fg.f139.com; Expires=Tue, 19 Jan 2038 03:14:07 GMT;");
		cookie.setCookie(ls);
		excelService.getAllPlasticExcelByCrawl();
	}
}
