package com.project.app.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 
* @ClassName: DataGrid 
* @Description: easyui datagrid数据格式
* @author luoxiao
* @date 2019年10月21日 上午10:33:15 
* 
* @param <T>
 */
public class DataGrid<T> implements Serializable{
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	private long total;
	
	private List<T> rows;
	
	public DataGrid(long total, List<T> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}
		
	public long getTotal() {
		return total;
	}
	
	public void setTotal(long total) {
		this.total = total;
	}
	
	public List<T> getRows() {
		return rows;
	}
	
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	
}
