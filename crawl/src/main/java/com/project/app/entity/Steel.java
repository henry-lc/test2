package com.project.app.entity;

import java.io.Serializable;

public class Steel implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	/**分类*/
	private Integer categoryID;
	/**类名*/
	private String categoryName;
	/**频道*/
	private Integer channelID;
	/**描述*/
	private String description;
	/**标题*/
	private String title;
	/**更新时间*/
	private String updateTime;

	public Integer getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getChannelID() {
		return channelID;
	}

	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "Steel [categoryID=" + categoryID + ", categoryName=" + categoryName + ", channelID=" + channelID
				+ ", description=" + description + ", title=" + title + ", updateTime=" + updateTime + "]";
	}	

}
