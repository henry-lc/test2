package com.project.app.cookie;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
* @ClassName: CookieEntity 
* @Description: Cookie设置类
* @author luoxiao
* @date 2019年10月16日 上午8:47:12 
*
 */
public class CookieEntity implements CookieHandle{
	
	private static CookieEntity cookieEntity = null;
	
	private static ReentrantLock lock = new ReentrantLock();
	
	private List<String> cookie = new ArrayList<String>();
	
	private static void syncInit() {
		lock.lock();
		if(cookieEntity == null) {
			cookieEntity = new CookieEntity();
		}
		lock.unlock();
	}
	
	public static CookieEntity getInstance() {
		if(cookieEntity == null) {
			syncInit();
		}
		return cookieEntity;
	}

	public  List<String> getCookie() {
		return cookie;
	}

	public  void setCookie(List<String> cookie) {
		this.cookie = cookie;
	}
	
	public static boolean isEmpty() {
		if(cookieEntity == null || cookieEntity.getCookie() == null || cookieEntity.getCookie().size() <=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<String> getCookieList() {
		return getInstance().getCookie();
	}

}
