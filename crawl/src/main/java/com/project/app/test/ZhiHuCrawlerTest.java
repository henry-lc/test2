package com.project.app.test;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;
import com.alibaba.fastjson.JSONObject;
import com.project.app.cookie.CookieEntity;
import com.project.app.util.FreemarkerUtils;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ZhiHuCrawlerTest
 * @Description 爬取知乎测试
 * @Author Henry
 * @Date 2019/11/23 11:33
 * @Version V1.0
 **/
public class ZhiHuCrawlerTest extends BreadthCrawler {

    public ZhiHuCrawlerTest(String crawlPath, boolean autoParse) {
        super(crawlPath, autoParse);

        this.addSeed("https://www.zhihu.com/");

        this.setThreads(1);

        this.setRequester(new ZhiHuCrawlerRequestCookie());

    }

    @Override
    public void visit(Page page, CrawlDatums crawlDatums) {
        this.parseHtml(page);
        //根目录
//        File file = new File("E:\\crawlZhiHu");
//        if (!file.exists()){
//            file.mkdir();
//        }
//        String html = page.html();
//
//        Document doc = page.doc();
//        Elements elements = doc.select("script");
//        List<String> srcs = new ArrayList<>();
//        for (Element e:elements){
//            //scrip文件夹
//            File scriptFiles = new File(file+"\\script");
//            if (!scriptFiles.exists()){
//                scriptFiles.mkdir();
//            }
//            String scriptUrl =  e.attr("src");
//            srcs.add(scriptUrl);
//            try {
//                URL url = new URL(scriptUrl);
//                URLConnection urlConnection = url.openConnection();
//                InputStream  is = urlConnection.getInputStream();
//
//                scriptUrl = scriptUrl.substring(scriptUrl.lastIndexOf("/")+1);
//
//                File scriptFile = new File(scriptFiles + "\\" + scriptUrl );
//                OutputStream out = new FileOutputStream(scriptFile);
//                int len= 0;
//                while ((len = is.read()) !=-1){
//                   out.write(len);
//                }
//            } catch (MalformedURLException ex) {
//                ex.printStackTrace();
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }
//
//        Elements  styleElement = doc.select("link");
//        List<String> styles = new ArrayList<>();
//        for (Element element : styleElement){
//            File styleFiles = new File(file +"\\style");
//            if (!styleFiles.exists()){
//                styleFiles.mkdir();
//            }
//            String styleUrl = element.attr("href");
//            styles.add(styleUrl);
//            try {
//                URL url = new URL(styleUrl);
//                URLConnection urlConnection = url.openConnection();
//                InputStream  is = urlConnection.getInputStream();
//
//                styleUrl = styleUrl.substring(styleUrl.lastIndexOf("/")+1);
//
//                File scriptFile = new File(styleFiles + "\\" + styleUrl );
//                OutputStream out = new FileOutputStream(scriptFile);
//                int len= 0;
//                while ((len = is.read()) !=-1){
//                    out.write(len);
//                }
//            } catch (MalformedURLException ex) {
//                ex.printStackTrace();
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//
//        }
//        try {
//            OutputStream out =  new FileOutputStream(file+"\\ZhiHuHtml.html");
//            byte[] b = html.getBytes();
//            out.write(b);
//            out.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public static void main(String[] args) throws Exception {
        List<String> ls = new ArrayList<String>();
        CookieEntity cookie = CookieEntity.getInstance();
        ls.add("q_c1=abf407334cc94962982d362a24d06566|1574479322|1574479322; _zap=1038b5a9-776b-4714-8eff-f2b56ebaa208; _xsrf=6bec7ee6-80e7-4fec-96e2-64ecbba880eb; d_c0=\"ACDu_aGlZRCPTkB91OJTkuQ3MakJhwiY1G8=|1574479321\"; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1574478025; tst=r; q_c1=d04200a8abdb45e5bd8d93e0cba3e0b1|1574488619000|1574488619000; tgw_l7_route=dee9fd64dc49915f4ffe487a64034d4c; capsion_ticket=\"2|1:0|10:1574493198|14:capsion_ticket|44:MmVkYWQwMWU4NDU2NGRiN2E2MjViZjg1YWY1ZDY5ZjE=|8ac7cae8311b86dcc8c188ec8f9a2a2bd0a8a6929c425860aa4a1022d7d5850d\"; l_n_c=1; r_cap_id=\"NzI2YWNlYjI3YzJhNDExNTljOGM5ODQ3N2ZhNjUwZTA=|1574493200|7c0fa65650a29f4b97be93aca2a0140adb2b5df1\"; cap_id=\"MjczMmE0YzkzOTc5NGVjNTg1OTU5NTRjNGM4ZmExNWI=|1574493200|33acfca979a7b94dced518258e865a424f6f66ea\"; l_cap_id=\"YTgxNzE4ZDg4NWVhNGUzYzg1NjFmNjJmNTQwOTIzODE=|1574493200|83d3698cd112ebe4e7ad635b4b005f352753f1c5\"; n_c=1; z_c0=Mi4xUW9YYUJBQUFBQUFBSU83OW9hVmxFQmNBQUFCaEFsVk5GQ3JHWGdCVHJLd1I4YTRlTW1xUVFsV18yN0xjWHlZejl3|1574493204|06b3a88bef2bcae5ee477034bcbbaca54006ffcc; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1574493207");
        cookie.setCookie(ls);

        ZhiHuCrawlerTest zhiHuCrawlerTest = new ZhiHuCrawlerTest("crawlPath",true);
        zhiHuCrawlerTest.start(1);

    }


    private String parseHtml(Page page){
        Document doc =  page.doc();
        Elements elements = doc.select("div.TopstoryItem-isRecommend");
        List<Map<String,String>> list = new ArrayList<>();
        elements.forEach(a->{
            Map<String,String> map = new HashMap<>();
            String title = a.select("h2.ContentItem-title>div>a").text();
            String content = a.select("div.RichContent>div.RichContent-inner>span").text();
            map.put("title",title);
            map.put("content",content);
            list.add(map);
        });
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("map", list);
        FreemarkerUtils.createDoc(result, "test.ftl", "D:/template/废钢", "测试.doc");

        return null;
    }


}
