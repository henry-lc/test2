package com.project.app.test;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatum;
import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;
import cn.edu.hfut.dmic.webcollector.util.RegexRule;
import com.alibaba.fastjson.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;

public class FirstCrawlerTest2 extends BreadthCrawler {

    private final static String crawlPath = "/Users/henry/Desktop/crawlPath";

    private final static String seed = "https://www.jianshu.com/u/e39da354ce50";

    RegexRule regexRule = new RegexRule();

    public FirstCrawlerTest2() {
        super(crawlPath, false);

        //添加爬取种子，根据自己的需求
        CrawlDatum datum = new CrawlDatum(seed).meta("depth",1);
        addSeed(datum);

        //设置线程数
        setThreads(1);

        //设置正则表达式规则
        regexRule.addRule("http://.*");

    }

    @Override
    public void visit(Page page, CrawlDatums crawlDatums) {
        File file = new File("e:\\test.html");
        String html = page.html();
        System.out.println("html:"+html);

        try {
            OutputStream out = new FileOutputStream(file);
            byte[] b = html.getBytes();
            out.write(b);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        String url = page.url();
        System.out.println("url:"+url);
        Document doc= page.doc();
        System.out.println("doc:" + JSONObject.toJSONString(doc));

        String name = page.select("a.name").text();
        System.out.println("name:"+name);

        Element element = page.select("div.title>a.name").first();
        System.out.println("element:"+ JSONObject.toJSONString(element));

        String name2 = element.text();
        System.out.println("name2:"+name2);
    }


    public static void main(String[] args) {
        try {
            for (int i=0; i<=5;i++){
                FirstCrawlerTest2 firstCrawlerTest2 = new FirstCrawlerTest2();
                //设置重复失败之后重新爬取次数
                firstCrawlerTest2.start(2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
