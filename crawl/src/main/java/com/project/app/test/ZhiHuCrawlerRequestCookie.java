package com.project.app.test;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatum;
import cn.edu.hfut.dmic.webcollector.plugin.net.OkHttpRequester;
import com.project.app.cookie.CookieEntity;
import okhttp3.Request;

/**
 * @ClassName ZhiHuCrawlerRequestCookie
 * @Description
 * @Author Henry
 * @Date 2019/11/23 14:36
 * @Version V1.0
 **/
public class ZhiHuCrawlerRequestCookie extends OkHttpRequester {

    private String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";

    @Override
    public Request.Builder createRequestBuilder(CrawlDatum crawlDatum) {
        String cookie = CookieEntity.getInstance().getCookie().get(0);
        return super.createRequestBuilder(crawlDatum)
                .addHeader("User-Agent", userAgent)
                .addHeader("Cookie", cookie);
    }
}
