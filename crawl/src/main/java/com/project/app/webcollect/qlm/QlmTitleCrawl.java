package com.project.app.webcollect.qlm;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.project.app.entity.QlmEntity;
import com.project.app.http.HttpURLConnectionHelper;

public class QlmTitleCrawl {

	public static List<QlmEntity> results = new ArrayList<QlmEntity>(); 
	
	public static void request(String url) {
		String requestResult = HttpURLConnectionHelper.sendRequest(url, "POST");
		JSONObject jsonObject = JSON.parseObject(requestResult);
		JSONObject data = jsonObject.getJSONObject("data");
		if(data != null) {
			JSONArray objs = data.getJSONArray("data");
			results.addAll(JSONObject.parseArray(objs.toJSONString(), QlmEntity.class));
		}
	}
}
