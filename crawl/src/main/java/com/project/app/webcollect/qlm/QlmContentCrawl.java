package com.project.app.webcollect.qlm;

import org.jsoup.nodes.Document;

import com.project.app.entity.QlmEntity;
import com.project.app.webcollect.QlmRequestCookie;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

public class QlmContentCrawl extends BreadthCrawler{

	public QlmContentCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(10);
		//设置请求cookie
		this.setRequester(new QlmRequestCookie());
	}

	@Override
	public void visit(Page page, CrawlDatums next) {	
		Document doc = page.doc();
		String content = doc.select("div.project-follow").get(0).html();
		String params = page.url().split("\\?")[1];
		String[] id = params.split("=");
		for(QlmEntity qlm : QlmTitleCrawl.results) {
			if(qlm.getContentid().equals(id[1])) {
				qlm.setContent(content);	
				break;
			}
		}
//		//装载头部+table+尾部代码
//		StringBuffer body = new StringBuffer();
//		
//		StringBuffer header = new StringBuffer();
//		header.append("<html class=\"pixel-ratio-1\" style=\"font-size: 96px;\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=GBK\">");
//		header.append("</head><body>");
//		header.append("<link rel=\"stylesheet\" href=\"../css/warn.min.css\">");
//		header.append("<meta name=\"viewport\" content=\"width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no\">");
//		header.append("<title>"+doc.select("title").get(0).html()+"</title>");
//		header.append("<meta name=\"applicable-device\" content=\"mobile\">");
//		header.append("<link rel=\"stylesheet\" href=\"../css/saved_resource\">");
//		header.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/index.css\">");
//		header.append("<div class=\"page page-current\" style=\"display:block;\">");
//		header.append("<div class=\"content niZJXMDetails\" id=\"content\">");
//		header.append("<div class=\"list-container\">");
//		header.append("<div class=\"item-inner details-title\">");
//		header.append("<div class=\"item-title\"></div>");
//		Element titleElement = doc.select("div.item-title").get(0);
//		String title = titleElement.html();
//		header.append(title);
//		header.append("</div>");
//		header.append("<div class=\"details-item-info\"><table class=\"trip\"><tbody>");
//		header.append("<tr class=\"trip-tr\"><td width=\"30%\" align=\"center\" class=\"zi_lei trip-td\">更新时间</td>");
//		Element dateElement = doc.select("td.trip-td").get(1);
//		String date = dateElement.html();
//		header.append("<td width=\"70%\" class=\"trip-td\" align=\"left\"><span>"+date+"</span></td>");
//		header.append("</tr></tbody></table></div>");
//		
//		header.append("<div class=\"project-follow\">");
//
//		StringBuffer footer = new StringBuffer();
//		footer.append("</div></div></div></div>");
//		footer.append("<script type=\"text/javascript\" src=\"../css/zepto.js\" charset=\"utf-8\"></script>");
//		footer.append("<script type=\"text/javascript\" src=\"../css/index.js\" charset=\"utf-8\"></script>");
//		footer.append("</body></html>");
//		
//		Element divContent = doc.select("div.project-follow").get(0);
//		String content = divContent.html();
//		body.append(header).append(content).append(footer);
//		
//		CrawlEntity crawlEntity = SpringUtils.getBean(CrawlEntity.class);
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//		String dir = sdf.format(Calendar.getInstance().getTime());
//		String params = page.url().split("\\?")[1];
//		String[] id = params.split("=");
//		String fileName = id[1]+".html";
//		DownloadUtils.TextToFile(crawlEntity.getStoreDir()+dir+"/"+fileName,body.toString().replace("\n", "\r\n"));
	}	
}
