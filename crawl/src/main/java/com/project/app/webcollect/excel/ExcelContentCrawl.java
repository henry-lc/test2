package com.project.app.webcollect.excel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.project.app.constant.CrawlDeputy;
import com.project.app.constant.FilterConstant;
import com.project.app.entity.ExcelEntity;
import com.project.app.webcollect.RequestCookie;
import com.project.app.webcollect.collector.DataCache;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

/**
 * 
* @ClassName: ExcelContentCrawl 
* @Description: 爬取Excel内容
* @author luoxiao
* @date 2019年10月18日 上午11:44:12 
*
 */
public class ExcelContentCrawl extends BreadthCrawler{
		
	public ExcelContentCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);	
		//设置线程数
		this.setThreads(1);
		//设置请求cookie
		this.setRequester(new RequestCookie());
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc = page.doc();
		Elements  tableTrs = doc.select("table").get(0).select("tbody tr");
		List<ExcelEntity> list = null;
		String url = page.url();
		if(url.indexOf("vid=75") > -1) {
			list = crawlSteel(tableTrs);
			stroeExcelData(CrawlDeputy.CrawlExcelEnum.STEEL_EXCEL.getCode(),list);
		}else if(url.indexOf("vid=16") > -1) {
			list = crawlCopper(tableTrs);
			stroeExcelData(CrawlDeputy.CrawlExcelEnum.COPPER_EXCEL.getCode(),list);
		}else if(url.indexOf("vid=17") > -1) {
			list = crawlAllaluminum(tableTrs);
			stroeExcelData(CrawlDeputy.CrawlExcelEnum.ALLALUMINUM_EXCEL.getCode(),list);
		}else if(url.indexOf("vid=137") > -1) {
			list = crawlCell(tableTrs);
			stroeExcelData(CrawlDeputy.CrawlExcelEnum.CELL_EXCEL.getCode(),list);
		}else if(url.indexOf("vid=689") > -1) {
			list = crawlPlastic(tableTrs);
			stroeExcelData(CrawlDeputy.CrawlExcelEnum.PLASTIC_EXCEL.getCode(),list);
		}
	}
	
	private List<ExcelEntity> crawlPlastic(Elements tableTrs){
		ExcelEntity excelEntity = null;
		List<ExcelEntity> list = new ArrayList<ExcelEntity>();
		for(int i=1;i<tableTrs.size()-2;i++) {
			Element tr = tableTrs.get(i);
			String productName = tr.select("td").get(0).text();
			String distract = tr.select("td").get(1).text();
			String price = tr.select("td").get(4).text();
			String upDown = tr.select("td").get(5).text(); 
			String date = tr.select("td").get(6).text(); 
			String upDownFlag = null;
			try {
				upDownFlag = tr.select("td").get(5).select("font").get(0).attr("class");
			}catch(Exception e) {
				upDownFlag = "";
			}
			String category = tr.select("td").get(2).text();

			if(upDownFlag != null && "down".equals(upDownFlag)) {
				upDown = "↓"+upDown;
			}else if(upDownFlag != null && "up".equals(upDownFlag)) {
				upDown = "↑"+upDown;
			}
			excelEntity = new ExcelEntity();
			excelEntity.setProductName(productName);
			excelEntity.setDistract(distract);
			excelEntity.setPrice(price);
			excelEntity.setUpDown(upDown);
			excelEntity.setCategory(category);
			excelEntity.setDate(date);
			list.add(excelEntity);
		}
		return list;
	}
	
	private List<ExcelEntity> crawlSteel(Elements tableTrs){
		ExcelEntity excelEntity = null;
		List<ExcelEntity> list = new ArrayList<ExcelEntity>();
		for(int i=1;i<tableTrs.size()-2;i++) {
			Element tr = tableTrs.get(i);
			Elements tds = tr.select("td");
			excelEntity = new ExcelEntity();
			String productName = tds.get(0).text();
			String distract = tds.get(1).text();
			String category = tds.get(2).text();
			String specs = tds.get(3).text();
			String tax = tds.get(4).text();
			String price = tds.get(5).text();
			String upDownFlag = null;
			String upDown = null;
			Integer flag = 0;
			List<String> filters = Arrays.asList(FilterConstant.filterAlloy);
			for(String filter : filters) {
				if(productName.toLowerCase().indexOf(filter.toLowerCase()) > -1) {
					flag = 1;
					break;
				}
			}
			if(flag == 1) {
				continue;
			}
			try {
				upDownFlag = tds.get(6).select("font").get(0).attr("class");
			}catch(Exception e) {
				upDownFlag = "";
			}
			upDown = tds.get(6).text();
			String dateTime = tds.get(7).text();
			excelEntity.setDistract(distract);
			if("－　　".equals(specs.trim()) || "－　".equals(specs.trim()) || "－".equals(specs.trim()) || "".equals(specs.trim())) {
				excelEntity.setProductName(productName);
			}else {
				excelEntity.setProductName(productName+" "+specs);
			}
			
			excelEntity.setPrice(price);
			if(upDownFlag != null && "down".equals(upDownFlag)) {
				excelEntity.setUpDown("↓"+upDown);
			}else if(upDownFlag != null && "up".equals(upDownFlag)) {
				excelEntity.setUpDown("↑"+upDown);
			}else {
				excelEntity.setUpDown(upDown);
			}
			excelEntity.setTax(tax);
			excelEntity.setCategory(category);
			excelEntity.setDate(dateTime);
			list.add(excelEntity);
		}
		return list;
	}
	
	private List<ExcelEntity> crawlAllaluminum(Elements tableTrs){
		ExcelEntity excelEntity = null;
		List<ExcelEntity> list = new ArrayList<ExcelEntity>();
		for(int i=1;i<tableTrs.size()-2;i++) {
			Element tr = tableTrs.get(i);
			Elements tds = tr.select("td");
			excelEntity = new ExcelEntity();
			String productName = tds.get(0).text();
			String distract = tds.get(1).text();
			String specs = tds.get(2).text();
			String category = tds.get(3).text();
			String price = tds.get(4).text();
			String upDownFlag = null;
			String upDown = null;
			try {
				upDownFlag = tds.get(5).select("font").get(0).attr("class");
			}catch(Exception e) {
				upDownFlag = "";
			}
			upDown = tds.get(5).text();
			String dateTime = tds.get(6).text();
			excelEntity.setDistract(distract);
			if("－　　".equals(specs.trim()) || "－　".equals(specs.trim()) || "－".equals(specs.trim()) || "".equals(specs.trim())) {
				excelEntity.setProductName(productName);
			}else {
				excelEntity.setProductName(productName+" "+specs);
			}
			excelEntity.setPrice(price);
			if(upDownFlag != null && "down".equals(upDownFlag)) {
				excelEntity.setUpDown("↓"+upDown);
			}else if(upDownFlag != null && "up".equals(upDownFlag)) {
				excelEntity.setUpDown("↑"+upDown);
			}else {
				excelEntity.setUpDown(upDown);
			}
			excelEntity.setCategory(category);
			excelEntity.setDate(dateTime);
			list.add(excelEntity);
		}
		return list;
	}
	
	private List<ExcelEntity> crawlCopper(Elements tableTrs){
		ExcelEntity excelEntity = null;
		List<ExcelEntity> list = new ArrayList<ExcelEntity>();
		for(int i=1;i<tableTrs.size()-2;i++) {
			Element tr = tableTrs.get(i);
			Elements tds = tr.select("td");
			excelEntity = new ExcelEntity();
			String productName = tds.get(0).text();
			String distract = tds.get(1).text();
			String specs = tds.get(2).text();
			String category = tds.get(3).text();
			String tax = tds.get(4).text();
			String price = tds.get(5).text();
			String upDownFlag = null;
			String upDown = null;
			try {
				upDownFlag = tds.get(6).select("font").get(0).attr("class");
			}catch(Exception e) {
				upDownFlag = "";
			}
			upDown = tds.get(6).text();
			String dateTime = tds.get(7).text();
			excelEntity.setDistract(distract);
			if("－　　".equals(specs.trim()) || "－　".equals(specs.trim()) || "－".equals(specs.trim()) || "".equals(specs.trim())) {
				excelEntity.setProductName(productName);
			}else {
				excelEntity.setProductName(productName+" "+specs);
			}
			excelEntity.setPrice(price);
			if(upDownFlag != null && "down".equals(upDownFlag)) {
				excelEntity.setUpDown("↓"+upDown);
			}else if(upDownFlag != null && "up".equals(upDownFlag)) {
				excelEntity.setUpDown("↑"+upDown);
			}else {
				excelEntity.setUpDown(upDown);
			}
			excelEntity.setTax(tax);
			excelEntity.setCategory(category);
			excelEntity.setDate(dateTime);
			list.add(excelEntity);
		}
		return list;
	}
	
	private List<ExcelEntity> crawlCell(Elements tableTrs){
		ExcelEntity excelEntity = null;
		List<ExcelEntity> list = new ArrayList<ExcelEntity>();
		for(int i=1;i<tableTrs.size()-2;i++) {
			Element tr = tableTrs.get(i);
			Elements tds = tr.select("td");
			excelEntity = new ExcelEntity();
			String productName = tds.get(0).text();
			String distract = tds.get(1).text();
			String category = tds.get(2).text();
			String price = tds.get(3).text();
			String upDownFlag = null;
			String upDown = null;
			try {
				upDownFlag = tds.get(4).select("font").get(0).attr("class");
			}catch(Exception e) {
				upDownFlag = "";
			}
			upDown = tds.get(4).text();
			String dateTime = tds.get(5).text();
			excelEntity.setDistract(distract);
			excelEntity.setProductName(productName);
			excelEntity.setPrice(price);
			if(upDownFlag != null && "down".equals(upDownFlag)) {
				excelEntity.setUpDown("↓"+upDown);
			}else if(upDownFlag != null && "up".equals(upDownFlag)) {
				excelEntity.setUpDown("↑"+upDown);
			}else {
				excelEntity.setUpDown(upDown);
			}
			excelEntity.setCategory(category);
			excelEntity.setDate(dateTime);
			list.add(excelEntity);
		}
		return list;
	}
	
	
	private void stroeExcelData(String key,List<ExcelEntity> newDatas) {
		DataCache dataCache = DataCache.getInstance();
		List<ExcelEntity> excelEntitys = dataCache.getExcelMaps().get(key);
		if(excelEntitys != null && excelEntitys.size()>0) {
			List<ExcelEntity> oldDatas = dataCache.getExcelMaps().get(key);
			oldDatas.addAll(newDatas);
		}else {
			dataCache.getExcelMaps().put(key, newDatas);
		}
	}
	
}
