package com.project.app.webcollect.excel;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.project.app.entity.ExcelEntity;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

public class CopperExcelContent extends BreadthCrawler{

	public static List<ExcelEntity> lists = new ArrayList<ExcelEntity>();
	
	public CopperExcelContent(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(1);
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc = page.doc();
		Elements trs = doc.select("table.message_table").get(0).select("tbody tr");
		ExcelEntity excel = null;
		for(int i=1;i<trs.size();i++) {
			excel = new ExcelEntity();
			Element tr = trs.get(i);
			Elements tds = tr.select("td");
			String productName = tds.get(0).text()+tds.get(1).text();
			String price = tds.get(2).text();
			String upDown = tds.get(4).text(); 
			if("".equals(upDown) || "0".equals(upDown)) {
				upDown = "平";
			}else if(upDown.indexOf("-")>-1) {
				upDown = "↓"+upDown.replace("-", "");
			}else {
				upDown = "↑"+upDown;
			}
			String distract = tds.get(5).text();
			String date = tds.get(6).text();
			String tax = tds.get(7).text();
			excel.setProductName(productName);
			excel.setPrice(price);
			excel.setUpDown(upDown);
			excel.setDate(date);
			excel.setTax(tax);
			excel.setDistract(distract.replace("地区", ""));
			lists.add(excel);
		}
	}
	
}
