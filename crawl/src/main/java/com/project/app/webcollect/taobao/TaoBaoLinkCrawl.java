package com.project.app.webcollect.taobao;


import org.jsoup.nodes.Document;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.berkeley.BreadthCrawler;

public class TaoBaoLinkCrawl extends BreadthCrawler{

	public static Integer pagenum = 1;
	
	public TaoBaoLinkCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(1);
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc = page.doc();
		Integer pageTotal = Integer.valueOf(doc.select("em.page-total").get(0).text());
		pagenum = pageTotal;
	}

}
