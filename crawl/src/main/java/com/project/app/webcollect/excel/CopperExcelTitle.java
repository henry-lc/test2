package com.project.app.webcollect.excel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

/**
 * 
* @ClassName: CopperExcelTitle 
* @Description: copper.ccmn.cn
* @author luoxiao
* @date 2019年10月24日 下午1:47:37 
*
 */
public class CopperExcelTitle extends BreadthCrawler{
	
	public static List<String> urls = new ArrayList<String>();

	private static String prefix = "https://copper.ccmn.cn";
	
	public CopperExcelTitle(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(1);
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc = page.doc();
		Elements trs = doc.select("table").get(0).select("tbody").get(0).select("tr");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String nowDate = sdf.format(Calendar.getInstance().getTime());
		for(Element tr : trs) {
			String url = tr.select("td").get(1).select("a").attr("href");
			String dateTime = tr.select("td").get(2).text().trim();
			if(nowDate.equals(dateTime)) {
				urls.add(prefix+url);
				break;
			}
		}
	}
}
