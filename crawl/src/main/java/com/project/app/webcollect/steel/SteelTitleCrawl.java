package com.project.app.webcollect.steel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.project.app.webcollect.RequestCookie;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

/**
 * 
* @ClassName: SteelTitleCrawl 
* @Description: 废钢标题爬取
* @author luoxiao
* @date 2019年10月19日 上午9:41:12 
*
 */
public class SteelTitleCrawl extends BreadthCrawler{

	private List<String> urls;
	
	private final static String subfixUrl = "http://fg.f139.com";
		
	public SteelTitleCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(1);
		//设置请求cookie
		this.setRequester(new RequestCookie());
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc = page.doc();
		String url = page.url();
		String[] params = url.split("\\?")[1].split("&");
		String paramCate = null;
		for(String param : params) {
			if(param.indexOf("categoryID")>-1) {
				paramCate = param;
				break;
			}
		}
		Element ulElement = doc.select("ul#channel").get(0);
		Elements liElements = ulElement.select("li");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String nowDate = sdf.format(Calendar.getInstance().getTime());
		List<String> lists = new ArrayList<String>();
		for(Element li : liElements) {
			String dateTime = li.select("span").get(0).text();
			if(nowDate.indexOf(dateTime) == -1) {
				break;
			}
			String href = li.select("span").get(1).select("a").attr("href");
			lists.add(subfixUrl+href+"?"+paramCate);
		}
		this.setUrls(lists);
	}

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

}
