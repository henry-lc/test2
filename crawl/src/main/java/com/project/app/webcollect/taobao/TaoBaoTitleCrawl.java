package com.project.app.webcollect.taobao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import com.project.app.util.WebUtils;



public class TaoBaoTitleCrawl{

	public static List<String> getTaoBaoTitleCrawl(Integer page){
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.DAY_OF_MONTH, 10);
		String targetDate = sdf.format(calendar.getTime());
		String url = "https://sf.taobao.com/item_list.htm?spm=a213w.7398504.pagination.1.57d83930rqnj6U&auction_source=0&sorder=1&st_param=-1&auction_start_seg=0&q=%C9%E8%B1%B8";
		url = url + "&auction_start_from="+targetDate+"&auction_start_to="+targetDate;
		List<String> urls = new ArrayList<String>();
		for(int i=1;i<=page;i++) {
			String targetUrl = url + "&page="+i;
			Document doc = WebUtils.getHtmlDocument(targetUrl);
			Elements lis = doc.select("li.pai-status-todo");
			for(Element li : lis) {
				String target = li.select("a.link-wrap").attr("href");
				target = target.replace("//sf-item", "https://susong-item");
				urls.add(target);
			}
		}
		return urls;
	}
	
	
}
