package com.project.app.webcollect.color;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.project.app.constant.FilterConstant;
import com.project.app.webcollect.RequestCookie;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;

/**
 * 
* @ClassName: ColorTitleCrawl 
* @Description: 废有色标题连接爬取
* @author luoxiao
* @date 2019年10月18日 上午9:23:56 
*
 */
public class ColorTitleCrawl extends BreadthCrawler{

	public static List<String> list = new ArrayList<String>();
	
	private final static String subfixUrl = "http://news.f139.com";
	
	public ColorTitleCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(1);
		//设置请求cookie
		this.setRequester(new RequestCookie());
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc = page.doc();	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String nowDate = sdf.format(Calendar.getInstance().getTime());
		//获取元素
		Element ulElement = doc.select("div#list").select("ul").get(0);
		Elements liElements = ulElement.select("li");
		for(Element li : liElements) {
			Elements span = li.select("span.cGray");
			String dateTime = span.get(0).text();
			if(dateTime.indexOf(nowDate) == -1){
				break;
			}
			String tilte = li.select("a").get(0).text();
			for(String filter : FilterConstant.filterColor){
				if(tilte.indexOf(filter) > -1) {
					String url = li.select("a").get(0).attr("href");
					list.add(subfixUrl+url);
					break;
				}
			}
		}
	}
}
