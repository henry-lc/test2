package com.project.app.webcollect.qlm.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.project.app.entity.QlmEntity;

public class QlmContentFilter {

	public static List<QlmEntity> filterPhone(List<QlmEntity> qlmLists){
		List<QlmEntity> lists = qlmLists.stream().filter(a->isMobile(a.getContent()) || isPhone(a.getContent()))
				.collect(Collectors.toList());
		return lists;
	}
	
	public static List<QlmEntity> filterDate(List<QlmEntity> qlmLists){
		List<QlmEntity> lists = qlmLists.stream().filter(a->isDate(a.getContent()))
				.collect(Collectors.toList());
		return lists;
	}
	
	/**
	 * 
	* @Title: isMobile 
	* @Description: 手机号码匹配 
	* @param @param str
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	private static boolean isMobile(String str) {
		String regex = "((13[0-9])|(14[5-8])|(15([0-3]|[5-9]))|(16[6])|(17[0|4|6|7|8])|(18[0-9])|(19[8-9]))\\d{8}";
		Pattern pattern = Pattern.compile(regex);;
		Matcher matcher = pattern.matcher(str);;
		boolean flag = matcher.find();
		return flag;
	}
	
	private static boolean isPhone(String str) {
	   Pattern p1 = null, p2 = null;
	   Matcher m = null;
	   boolean b = false;
	   p1 = Pattern.compile("[0][1-9]{2,3}-[0-9]{5,10}"); // 验证带区号的
	   p2 = Pattern.compile("[1-9]{1}[0-9]{5,8}");     // 验证没有区号的
	   if (str.length() > 9) {
	     m = p1.matcher(str);
	     b = m.find();
	   } else {
	     m = p2.matcher(str);
	     b = m.find();
	  }
	   return b;
	}
	
	private static boolean isDate(String str) {
		String regex = "\\d{4}(-|年|:|\\.){1}[1-12]{1,2}(-|月|:|\\.){1}[1-30]{1,2}(日){0,1}";
		Pattern date = Pattern.compile(regex);
		Matcher matcher = date.matcher(str);
		List<String> matchStrs = new ArrayList<String>();
		while (matcher.find()) { //此处find（）每次被调用后，会偏移到下一个匹配
           matchStrs.add(matcher.group());//获取当前匹配的值
        }
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy年MM月dd日");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy.MM.dd");
		String nowDate = sdf.format(Calendar.getInstance().getTime());
		for(String regexDate : matchStrs) {
			String formatDate = null;
			try{
				formatDate =sdf.format(sdf.parse(regexDate)); 
			}catch(Exception e) {
				try {
					formatDate =sdf.format(sdf1.parse(regexDate));
				} catch (ParseException e1) {
					try {
						formatDate =sdf.format(sdf2.parse(regexDate));
					} catch (ParseException e2) {
						try {
							formatDate =sdf.format(sdf3.parse(regexDate));
						} catch (ParseException e3) {
							continue;
						}
					} 
				} 
			}
			if(formatDate.compareTo(nowDate)>0) {
				return true;
			}
			
		}
		return false;
	}
}
