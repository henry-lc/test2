package com.project.app.webcollect.steel;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.project.app.constant.CrawlDeputy;
import com.project.app.entity.NewsEntity;
import com.project.app.webcollect.RequestCookie;
import com.project.app.webcollect.collector.DataCache;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatums;
import cn.edu.hfut.dmic.webcollector.model.Page;
import cn.edu.hfut.dmic.webcollector.plugin.rocks.BreadthCrawler;


/**
 * 
* @ClassName: SteelContentCrawl 
* @Description: 废钢内容爬取
* @author luoxiao
* @date 2019年10月19日 上午9:41:31 
*
 */
public class SteelContentCrawl extends BreadthCrawler{

	public SteelContentCrawl(String crawlPath, boolean autoParse) {
		super(crawlPath, autoParse);
		//设置线程数
		this.setThreads(20);
		//设置请求cookie
		this.setRequester(new RequestCookie());		
	}

	@Override
	public void visit(Page page, CrawlDatums next) {
		Document doc =Jsoup.parse(page.doc().html().replaceAll("&nbsp;", ""));

		String title = doc.select("div.detail").select("h1").get(0).text();
		String dateTime = doc.select("p.detail_time").get(0).text();
		String description = doc.select("div.article").get(0).text();
		dateTime = dateTime.replace("来源：富宝资讯", "");
		int position = description.indexOf("我要订阅 ");
		if(position>-1) {
			description = description.substring(0, position);
		}
		position = description.indexOf("富宝资讯免责声明");
		if(position>-1) {
			description = description.substring(0, position);
		}
		NewsEntity news = new NewsEntity();
		news.setTitle(title);
		news.setDateTime(dateTime.replaceAll(" +", ""));
		description = description.replaceAll("  +", "");
		if(description.indexOf("富宝资讯") == 0 && description.indexOf("： ")>-1) {
			description = description.substring(description.indexOf("： ")+1, description.length());
		}else if(description.indexOf("富宝资讯") == 0 && description.indexOf(": 　　")>-1) {
			description = description.substring(description.indexOf(": 　　")+1, description.length());
		}else if(description.indexOf("富宝资讯") == 0 && description.indexOf("：")>-1) {
			description = description.substring(description.indexOf("：")+1, description.length());
		}
		news.setDescription(description);
		
		String url = page.url();
		if(url.indexOf("categoryID=27")>-1) {
			stroeExcelData(CrawlDeputy.CrawlExcelEnum.STELL_PRICE_WORD.getCode(),news);
		}else if(url.indexOf("categoryID=832")>-1) {
			stroeExcelData(CrawlDeputy.CrawlExcelEnum.STELL_BASE_WORD.getCode(),news);
		}
	}
	
	private void stroeExcelData(String key,NewsEntity newDatas) {
		DataCache dataCache = DataCache.getInstance();
		List<NewsEntity> newsEntitys = dataCache.getWordMaps().get(key);
		if(newsEntitys != null && newsEntitys.size()>0) {
			newsEntitys.add(newDatas);
		}else {
			List<NewsEntity> addNews = new ArrayList<NewsEntity>();
			addNews.add(newDatas);
			dataCache.getWordMaps().put(key, addNews);
		}
			
	}

}
