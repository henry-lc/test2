package com.project.app.ocr;

import java.util.Map.Entry;

import com.project.app.constant.FilterConstant;
import com.project.app.ocr.handle.OCRHandle;

public class RecognitionImage {

	private OCRHandle ocrHandle;
	
	public OCRHandle getOcrHandle() {
		return ocrHandle;
	}

	public void setOcrHandle(OCRHandle ocrHandle) {
		this.ocrHandle = ocrHandle;
	}

	public String getContent(byte[] file) {
		return handlerContent(ocrHandle.read(file));
	}
	
	private  String handlerContent(String content) {
		for(Entry<String,String> entry : FilterConstant.map.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if(content.indexOf(key)>-1) {
				content = content.replaceAll(key, value);
			}
		}
		return content;
	}
}
