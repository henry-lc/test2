package com.project.app.ocr.handle;

public interface OCRHandle {

	public String read(byte[] file);
}
