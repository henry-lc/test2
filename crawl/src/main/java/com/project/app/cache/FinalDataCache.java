package com.project.app.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import com.project.app.entity.ExcelEntity;
import com.project.app.entity.NewsEntity;
import com.project.app.entity.QlmEntity;
import com.project.app.entity.TaoBaoEntity;


public class FinalDataCache {
	/**单利模式*/
	private  static FinalDataCache finalDataCache = null;
	/**防止并发重复新建对象*/
	private static ReentrantLock lock = new ReentrantLock();
	
	private Map<String,List<NewsEntity>> wordMaps = new ConcurrentHashMap<String, List<NewsEntity>>();
	
	private Map<String,List<ExcelEntity>> excelMaps = new  ConcurrentHashMap<String,List<ExcelEntity>>();
	
	private List<QlmEntity> lastQlmCache = new  ArrayList<QlmEntity>();
	
	private List<TaoBaoEntity> lasttaoBaoCache = new  ArrayList<TaoBaoEntity>();
	
	private static void syncInit(){
		lock.lock();
		if(finalDataCache == null){
			finalDataCache = new FinalDataCache();
		}
		lock.unlock();
	}
	
	public static FinalDataCache getInstance() {
		if(finalDataCache == null) {
			syncInit();
		}
		return finalDataCache;
	}

	public Map<String, List<NewsEntity>> getWordMaps() {
		return wordMaps;
	}

	public void setWordMaps(Map<String, List<NewsEntity>> wordMaps) {
		this.wordMaps = wordMaps;
	}

	public Map<String, List<ExcelEntity>> getExcelMaps() {
		return excelMaps;
	}

	public void setExcelMaps(Map<String, List<ExcelEntity>> excelMaps) {
		this.excelMaps = excelMaps;
	}

	public List<QlmEntity> getLastQlmCache() {
		return lastQlmCache;
	}

	public void setLastQlmCache(List<QlmEntity> lastQlmCache) {
		this.lastQlmCache = lastQlmCache;
	}

	public List<TaoBaoEntity> getLasttaoBaoCache() {
		return lasttaoBaoCache;
	}

	public void setLasttaoBaoCache(List<TaoBaoEntity> lasttaoBaoCache) {
		this.lasttaoBaoCache = lasttaoBaoCache;
	}
	
}
