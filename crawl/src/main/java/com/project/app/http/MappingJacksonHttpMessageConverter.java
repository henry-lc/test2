package com.project.app.http;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

public class MappingJacksonHttpMessageConverter extends MappingJackson2HttpMessageConverter {

	public MappingJacksonHttpMessageConverter() {
		List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.TEXT_PLAIN);
        mediaTypes.add(MediaType.TEXT_HTML); 
        setSupportedMediaTypes(mediaTypes);
	}

	
}
