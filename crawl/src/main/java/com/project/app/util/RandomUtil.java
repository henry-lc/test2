package com.project.app.util;

/**
 * @ClassName RandomUtil
 * @Description
 * @Author Henry
 * @Date 2019/12/13 11:14
 * @Version V1.0
 **/
public class RandomUtil {

    /**
     * 获取30-60之间的随机数
     * @return
     */
    public static int getRandom(){
        return (int)(60 +Math.random() * ( 180 - 60 + 1) );
    }
}
