package com.project.app.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class FreemarkerUtils {

	public static String createDoc(Map<?,?> dataMap,String templateName,String filePath,String fileName) {
		try {
			Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);
			configuration.setDefaultEncoding("UTF-8");
			configuration.setClassForTemplateLoading(FreemarkerUtils.class,"/templates");
			Template template = configuration.getTemplate(templateName);
			File outFile = new File(filePath+File.separator+fileName);
			if (!outFile.getParentFile().exists()){
	            outFile.getParentFile().mkdirs();
	        }
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
			template.process(dataMap, out);
			out.flush();
			out.close();
			return filePath+"/"+fileName;
		}catch(Exception e) {
			throw new RuntimeException("生成模板错误!");
		}
		
	}
}
