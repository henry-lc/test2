package com.project.app.util;

import java.util.Objects;
import java.util.function.BiConsumer;

public class ForEachUtils {

	public static <T> void forEach(int startIndex,Iterable<? extends T> elements, BiConsumer<Integer, ? super T> action) {
        Objects.requireNonNull(elements);
        Objects.requireNonNull(action);
        for (T element : elements) {
            action.accept(startIndex++, element);
        }
    }

}
