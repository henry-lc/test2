package com.project.app.constant;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

@Component
public class CrawlCache implements ApplicationRunner{

	@Value("classpath:crawl.json")
	private Resource crawls;
	
	public static JSONObject jsonObject = new JSONObject();
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		try {
			String areaData =  IOUtils.toString(crawls.getInputStream(), Charset.forName("UTF-8"));
			jsonObject = JSON.parseObject(areaData);
		 } catch (IOException e) {
			e.printStackTrace();
		}	
		
	}

}
