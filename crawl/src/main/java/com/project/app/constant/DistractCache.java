package com.project.app.constant;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 
* @ClassName: DistractCache 
* @Description:	地域分区
* @author luoxiao
* @date 2019年10月16日 上午11:18:35 
*
 */
@Component
public class DistractCache  implements ApplicationRunner{
	
	@Value("classpath:region.json")
	private Resource areaRes;
	
	public static Map<String,List<String>> distractMap = new HashMap<String, List<String>>();
	

	@SuppressWarnings("unchecked")
	@Override
	public void run(ApplicationArguments args) throws Exception {
		try {
			distractMap.clear();
			String areaData =  IOUtils.toString(areaRes.getInputStream(), Charset.forName("UTF-8"));
			JSONObject jsonObject = JSON.parseObject(areaData);
			Map<String,List<String>> parseObject = JSON.parseObject(jsonObject.toJSONString(), Map.class);
			distractMap.putAll(parseObject);
		 } catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	
	public static <T> Map<String, List<T>> subRegion(List<T> object,String param1,String param2){
		if(object == null || object.size() == 0) {
			throw new RuntimeException("暂时不存在更新的内容!");
		}
		Map<String, List<T>> map = new HashMap<String, List<T>>();

		List<T> subList = null;
		for(T t : object) {
			String regionName = getRegion(t,t.getClass(),param1,param2);
			if(map.get(regionName) == null) {
				subList = new ArrayList<T>();
			}else {
				subList = map.get(regionName);
			}
			subList.add(t);
			map.put(regionName, subList);
		}
		return map;
	}
	
	public static <T> String  getRegion(T t,String param1,String param2){
		if(t == null) {
			throw new RuntimeException("暂时不存在更新的内容!");
		}
		String regionName = getRegion(t,t.getClass(),param1,param2);
		return regionName;
	}
	
	public static <T> void subRegion(List<T> object,String param1,String param2,String param3){
		try {
			if(object == null || object.size() == 0) {
				throw new RuntimeException("暂时不存在更新的内容!");
			}
			for(T t : object) {
				String regionName = getRegion(t,t.getClass(),param1,param2);
				Method methodRegion =t.getClass().getDeclaredMethod(param3, String.class);
				methodRegion.invoke(t, regionName);
			}
		}catch(Exception e) {
			throw new RuntimeException(e);	
		}
		
	}
	
	//获取分区
	private static String getRegion(Object o,Class<?> clazz,String param1,String param2) {
		try {
			String title = null;
			String description = null;
			if(param1 != null) {
				Method methodTitle = clazz.getDeclaredMethod(param1, null);
				title = (String)methodTitle.invoke(o);
			}
			if(param2 != null) {
				Method methodDesc = clazz.getDeclaredMethod(param2, null);
				description = (String)methodDesc.invoke(o);
			}
			for(Entry<String,List<String>> entry : DistractCache.distractMap.entrySet()) {
				String region = entry.getKey();
				List<String> distracts = entry.getValue();
				if(title != null) {
					for(String distract : distracts) {
						if(title.indexOf(distract)>-1) {
							return region;
						}
					}
				}
				if(description != null) {
					for(String distract : distracts) {
						if(description.indexOf(distract)>-1) {
							return region;
						}
					}
				}
			}
			return "未知";
		} catch (Exception e) {
			throw new RuntimeException(e);	
		}
		
		
	}

}
