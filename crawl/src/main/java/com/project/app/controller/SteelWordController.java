package com.project.app.controller;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.cache.StartFlag;
import com.project.app.entity.NewsEntity;
import com.project.app.entity.ReturnType;
import com.project.app.service.SteelService;
import com.project.app.util.DataGridUtils;
import com.project.app.util.FileUtils;
import com.project.app.util.FreemarkerUtils;
import com.project.app.util.ReturnUtils;
import com.project.app.util.WordUtils;

@RequestMapping(value="steel/word")
@RestController
public class SteelWordController {

	@Autowired
	private SteelService steelService;
	
	
	@RequestMapping(value = "/getSteelBaseList")
	public ReturnType<?> getSteelBaseList(){
		try {
			List<NewsEntity> lists = steelService.getAllNewBaseByCache();			
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/getSteelBaseCrawl")
	public ReturnType<?> getSteelBaseCrawl(){
		try {
			List<NewsEntity> lists = steelService.getAllNewBaseByCrawl();
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value="/saveBase")
	public ReturnType<?> saveBase(NewsEntity newsEntity){
		try {
			if(newsEntity != null && newsEntity.getId() != null) {
				List<NewsEntity> lists = steelService.getAllNewBaseByCache();
				NewsEntity target = lists.stream().filter(s->s.getId().equals(newsEntity.getId())).collect(Collectors.toList()).get(0);
				target.setRegion(newsEntity.getRegion());
				return ReturnUtils.success("修改成功!");
			}
			throw new RuntimeException("修改对象为空!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
		
	}
	
	@RequestMapping(value = "/exportBase")
	public ResponseEntity<byte[]> exportBase(String id) throws UnsupportedEncodingException{
		List<NewsEntity> lists = steelService.getAllNewBaseByCache();
		String[] ids = id.split(",");
		List<NewsEntity> filter = lists.stream().filter(s->Arrays.asList(ids).contains(s.getId())).collect(Collectors.toList());
		filter.forEach(a -> {
			String  content = WordUtils.concatSteelWordFontColor(a.getDescription());
			a.setDescription(content);
		});
		Map<String,List<NewsEntity>> map = filter.stream().collect(Collectors.groupingBy(NewsEntity::getRegion));
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("map", map);
		SimpleDateFormat sdf = new SimpleDateFormat("MM月d日");
		result.put("nowDate", sdf.format(Calendar.getInstance().getTime()));
		String filename = new String(("废钢-基地"+System.currentTimeMillis()+".doc").getBytes("UTF-8"),"iso-8859-1");
		String path = FreemarkerUtils.createDoc(result, "废钢导出模板.ftl", "D:/template/废钢", filename);
		return FileUtils.download(path);
	}
	
	@RequestMapping(value = "/clearBaseCrawlCache")
	public ReturnType<?> clearCrawlCache(){
		try {
			StartFlag.wordBaseStop();
			return ReturnUtils.success("爬虫标识缓存清除成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/getSteelPriceList")
	public ReturnType<?> getSteelPriceList(){
		try {
			List<NewsEntity> lists = steelService.getAllNewPriceByCache();
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/getSteelPriceCrawl")
	public ReturnType<?> getSteelPriceCrawl(){
		try {
			List<NewsEntity> lists = steelService.getAllNewPriceByCrawl();
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value="/savePrice")
	public ReturnType<?> savePrice(NewsEntity newsEntity){
		try {
			if(newsEntity != null && newsEntity.getId() != null) {
				List<NewsEntity> lists = steelService.getAllNewPriceByCache();
				NewsEntity target = lists.stream().filter(s->s.getId().equals(newsEntity.getId())).collect(Collectors.toList()).get(0);
				target.setRegion(newsEntity.getRegion());
				return ReturnUtils.success("修改成功!");
			}
			throw new RuntimeException("修改对象为空!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/exportPrice")
	public ResponseEntity<byte[]> exportPrice(String id) throws UnsupportedEncodingException{
		List<NewsEntity> lists = steelService.getAllNewPriceByCache();
		String[] ids = id.split(",");
		List<NewsEntity> filter = lists.stream().filter(s->Arrays.asList(ids).contains(s.getId())).collect(Collectors.toList());
		filter.forEach(a -> {
			String  content = WordUtils.concatSteelWordFontColor(a.getDescription());
			a.setDescription(content);
		});
		Map<String,List<NewsEntity>> map = filter.stream().collect(Collectors.groupingBy(NewsEntity::getRegion));
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("map", map);
		SimpleDateFormat sdf = new SimpleDateFormat("MM月d日");
		result.put("nowDate", sdf.format(Calendar.getInstance().getTime()));
		String filename = new String(("废钢-调价"+System.currentTimeMillis()+".doc").getBytes("UTF-8"),"iso-8859-1");
		String path = FreemarkerUtils.createDoc(result, "废钢导出模板.ftl", "D:/template/废钢", filename);
		return FileUtils.download(path);
	}
	
	
	@RequestMapping(value = "/clearPriceCrawlCache")
	public ReturnType<?> clearPriceCrawlCache(){
		try {
			StartFlag.wordPriceStop();
			return ReturnUtils.success("爬虫标识缓存清除成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
}
