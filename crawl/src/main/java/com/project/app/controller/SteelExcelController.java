package com.project.app.controller;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.cache.StartFlag;
import com.project.app.constant.CityCache;
import com.project.app.constant.DistractCache;
import com.project.app.entity.ExcelEntity;
import com.project.app.entity.ReturnType;
import com.project.app.service.ExcelService;
import com.project.app.util.DataGridUtils;
import com.project.app.util.FileUtils;
import com.project.app.util.ForEachUtils;
import com.project.app.util.FreemarkerUtils;
import com.project.app.util.ReturnUtils;

@RequestMapping(value = "steel/excel")
@RestController
public class SteelExcelController {

	@Autowired
	private ExcelService excelService;
	
	
	@RequestMapping(value = "steelExcelListByCache")
	public ReturnType<?> steelExcelListByCache(String distract,String region,Integer pageIndex,Integer pageSize){
		try {
			List<ExcelEntity> excelEntitys = excelService.getAllSteelByCache();
			if(excelEntitys == null || excelEntitys.size() <= 0) {
				throw new RuntimeException("缓存数据不存在!");
			}
			if(StringUtils.isNotBlank(distract)) {
				List<String> province = CityCache.getCityByProvince(distract);
				if(province != null) {
					excelEntitys = excelEntitys.stream().filter(s->s.getProvince().indexOf(distract)>-1).collect(Collectors.toList());
				}else {
					excelEntitys = excelEntitys.stream().filter(s->s.getDistract().indexOf(distract)>-1).collect(Collectors.toList());
				}	
			}
			if(StringUtils.isNotBlank(region)) {
				excelEntitys = excelEntitys.stream().filter(s->s.getRegion().indexOf(region)>-1).collect(Collectors.toList());
			}
			Integer total = excelEntitys.size();
			if(pageIndex != null && pageSize != null) {
				Integer maxCount = pageIndex*pageSize;
				if(maxCount>=excelEntitys.size()) {
					excelEntitys = excelEntitys.subList((pageIndex-1)*pageSize, excelEntitys.size());
				}else {
					excelEntitys = excelEntitys.subList((pageIndex-1)*pageSize, maxCount);
				}	
			}
			return ReturnUtils.success(DataGridUtils.toDataGrid(excelEntitys,total), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "steelExcelListByCrawl")
	public ReturnType<?> steelExcelListByCrawl(Integer pageIndex,Integer pageSize){
		try {
			List<ExcelEntity> excelEntitys = excelService.getAllSteelByCrawl();
			Integer total = excelEntitys.size();
			if(pageIndex != null && pageSize != null) {
				excelEntitys = excelEntitys.subList((pageIndex-1)*pageSize, pageIndex*pageSize);
			}
			return ReturnUtils.success(DataGridUtils.toDataGrid(excelEntitys,total), "查询成功!");
		}catch(Exception e) {
			StartFlag.excelSteelStop();
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "saveSteel")
	public ReturnType<?> saveBase(ExcelEntity excelEntity){
		try {
			if(excelEntity != null && excelEntity.getId() != null) {
				List<ExcelEntity> lists = excelService.getAllSteelByCache();
				ExcelEntity target = lists.stream().filter(s->s.getId().equals(excelEntity.getId())).collect(Collectors.toList()).get(0);
				target.setRegion(excelEntity.getRegion());
				return ReturnUtils.success("修改成功!");
			}
			throw new RuntimeException("修改对象为空!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}	
	}
	
	@RequestMapping(value = "/clearSteelCrawlCache")
	public ReturnType<?> clearCrawlCache(){
		try {
			StartFlag.excelSteelStop();
			return ReturnUtils.success("爬虫标识缓存清除成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/exchange")
	public ReturnType<?> exchange(String id){
		try {
			List<ExcelEntity> lists = excelService.getAllSteelByCache();
			String[] ids = id.split(",");
			ForEachUtils.forEach(0, lists, (index,item)->{
				if(Arrays.asList(ids).contains(item.getId())) {
					String distract = item.getDistract();
					String productName = item.getProductName();
					item.setDistract(productName);
					item.setProductName(distract);
					String region = DistractCache.getRegion(item, null, "getDistract");
					item.setRegion(region);
				}
			});
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists,lists.size()),"互换成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/exportSteelExcel")
	public ResponseEntity<byte[]> exportBase(String id) throws UnsupportedEncodingException{
		List<ExcelEntity> lists = excelService.getAllSteelByCache();
		String[] ids = id.split(",");
		List<ExcelEntity> filter = lists.stream().filter(s->Arrays.asList(ids).contains(s.getId())).collect(Collectors.toList());
		Map<String,List<ExcelEntity>> map = filter.stream().sorted(Comparator.comparing(ExcelEntity::getProvince)).collect(Collectors.groupingBy(ExcelEntity::getRegion));
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("map", map);
		String filename = new String(("废钢"+System.currentTimeMillis()+".xls").getBytes("UTF-8"),"iso-8859-1");
		String path = FreemarkerUtils.createDoc(result, "steelTempalte.ftl", "D:/template/excel/废钢", filename);
		return FileUtils.download(path);
	}
	
}
