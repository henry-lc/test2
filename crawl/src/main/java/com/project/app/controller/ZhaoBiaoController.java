package com.project.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.entity.ReturnType;
import com.project.app.service.QlmService;
import com.project.app.service.TaoBaoService;
import com.project.app.util.ReturnUtils;

@RequestMapping("/zhaobiao")
@RestController
public class ZhaoBiaoController {

	@Autowired
	private QlmService qlmService;
	
	@Autowired
	private TaoBaoService taoBaoService;
	
	@RequestMapping("/qlm/crawl")
	public ReturnType<?> crawlQlm(){
		try {
			qlmService.saveToTarget();
			return ReturnUtils.success("爬取成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}		
	}
	
	@RequestMapping("/al/crawl")
	public ReturnType<?> crawlal(){
		try {
			taoBaoService.saveToTarget();
			return ReturnUtils.success("爬取成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}		
	}
}
