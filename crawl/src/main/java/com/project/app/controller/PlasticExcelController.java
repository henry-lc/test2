package com.project.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.cache.StartFlag;
import com.project.app.entity.ExcelEntity;
import com.project.app.entity.ReturnType;
import com.project.app.service.ExcelService;
import com.project.app.util.CopyUtils;
import com.project.app.util.DataGridUtils;
import com.project.app.util.FileUtils;
import com.project.app.util.ForEachUtils;
import com.project.app.util.FreemarkerUtils;
import com.project.app.util.ReturnUtils;

@RequestMapping(value="plastic/excel")
@RestController
public class PlasticExcelController {

	@Autowired
	private ExcelService excelService;
	
	@RequestMapping(value = "plasticExcelListByCrawl")
	public ReturnType<?> plasticExcelListByCrawl(Integer pageIndex,Integer pageSize){
		try {
			List<ExcelEntity> lists = excelService.getAllPlasticExcelByCrawl();
			Integer total = lists.size();
			if(pageIndex != null && pageSize != null) {
				Integer maxCount = pageIndex*pageSize;
				if(maxCount>=lists.size()) {
					lists = lists.subList((pageIndex-1)*pageSize, lists.size());
				}else {
					lists = lists.subList((pageIndex-1)*pageSize, maxCount);
				}	
			}
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists,total), "查询成功!");
		}catch(Exception e) {
			StartFlag.excelPlasticStop();
			return ReturnUtils.fail(e.getMessage());
		}
	} 
	
	@RequestMapping(value = "plasticExcelListByCache")
	public ReturnType<?> plasticExcelListByCache(Integer pageIndex,Integer pageSize){
		try {
			List<ExcelEntity> lists = excelService.getAllPlasticByCache();
			Integer total = lists.size();
			if(pageIndex != null && pageSize != null) {
				Integer maxCount = pageIndex*pageSize;
				if(maxCount>=lists.size()) {
					lists = lists.subList((pageIndex-1)*pageSize, lists.size());
				}else {
					lists = lists.subList((pageIndex-1)*pageSize, maxCount);
				}	
			}
			return ReturnUtils.success(DataGridUtils.toDataGrid(lists,total), "查询成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	} 
	
	@RequestMapping(value = "/clearPlasticCrawlCache")
	public ReturnType<?> clearCrawlCache(){
		try {
			StartFlag.excelPlasticStop();;
			return ReturnUtils.success("爬虫标识缓存清除成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/exportPlasticExcel")
	public ResponseEntity<byte[]> exportBase(String id) throws UnsupportedEncodingException{
		List<ExcelEntity> lists = excelService.getAllPlasticByCache();
		String[] ids = id.split(",");
		List<ExcelEntity> filter = lists.stream().filter(s->Arrays.asList(ids).contains(s.getId())).collect(Collectors.toList());
		SimpleDateFormat oldSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newSdf = new SimpleDateFormat("MM月dd日");
		List<ExcelEntity> exportList = null;
		try {
			exportList = CopyUtils.deepCopy(filter);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		ForEachUtils.forEach(0, exportList, (index,item)->{
			try {
				Date date = oldSdf.parse(item.getDate());
				String now = newSdf.format(date);
				item.setDate(now);
			} catch (ParseException e1) {
				e1.printStackTrace();
			} 
		});
		Map<String,List<ExcelEntity>> map = exportList.stream().sorted(Comparator.comparing(ExcelEntity::getTax)).collect(Collectors.groupingBy(ExcelEntity::getTax));
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("map", map);
		String filename = new String(("废塑料"+System.currentTimeMillis()+".xls").getBytes("UTF-8"),"iso-8859-1");
		String path = FreemarkerUtils.createDoc(result, "excelPlasticTempalte.ftl", "D:/template/excel/废塑料", filename);
		return FileUtils.download(path);
	}
}
