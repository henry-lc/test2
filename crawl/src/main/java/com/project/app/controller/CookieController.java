package com.project.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.cookie.CookieEntity;
import com.project.app.entity.ReturnType;
import com.project.app.util.ReturnUtils;

@RestController
@RequestMapping(value = "/cookie")
public class CookieController {

	@RequestMapping("/setCookie")
	@ResponseBody
	public ReturnType<?> setCookie(String jessionId,String qquc){
		try {
			CookieEntity cookieEntity = CookieEntity.getInstance();
			cookieEntity.getCookie().clear();
			List<String> cookieList = new ArrayList<String>();
			cookieList.add(jessionId);
			cookieList.add(qquc);
			cookieEntity.setCookie(cookieList);
			return ReturnUtils.success("cookie设置成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
	
	@RequestMapping("/getCookie")
	@ResponseBody
	public ReturnType<?> getCookie(){
		try {
			CookieEntity cookieEntity = CookieEntity.getInstance();
			return ReturnUtils.success(cookieEntity,"cookie获取成功!");
		}catch(Exception e) {
			return ReturnUtils.fail(e.getMessage());
		}
	}
}
