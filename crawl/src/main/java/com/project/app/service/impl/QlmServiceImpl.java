package com.project.app.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.project.app.util.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.project.app.cache.FinalDataCache;
import com.project.app.cache.StartFlag;
import com.project.app.config.CrawlEntity;
import com.project.app.constant.CrawlCache;
import com.project.app.constant.CrawlDeputy;
import com.project.app.entity.QlmEntity;
import com.project.app.http.HttpRequest;
import com.project.app.service.QlmService;
import com.project.app.util.CopyUtils;
import com.project.app.webcollect.qlm.QlmContentCrawl;
import com.project.app.webcollect.qlm.QlmTitleCrawl;
import com.project.app.webcollect.qlm.filter.QlmContentFilter;

@Service("qlmService")
public class QlmServiceImpl implements QlmService{

	@Autowired
	private CrawlEntity crawlEntity;

	@Autowired
	private HttpRequest httpRequest;

	@Override
	public void saveToTarget() {
		StartFlag.qlmIsStart();
		StartFlag.qlmStart();
		List<QlmEntity> qlms = crawlTarget(CrawlDeputy.CrawlExcelEnum.QLM_DATA.getCode());
		StartFlag.qlmStop();
		FinalDataCache cache = FinalDataCache.getInstance();
		List<QlmEntity> cacheQlms = cache.getLastQlmCache();
		List<QlmEntity> results = removeRepeat(qlms,cacheQlms);
		try {
			List<QlmEntity> copy = CopyUtils.deepCopy(qlms);
			cache.getLastQlmCache().clear();
			cache.setLastQlmCache(copy);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		String url = crawlEntity.getSaveUrl();
		results.stream().forEach(a->{
			a.setTitle(a.getPopTitle());
		});
		if(qlms != null && qlms.size()>0) {
			httpRequest.postJsonForObject(url, JSON.toJSONString(results));
		}
	}

	private List<QlmEntity> crawlTarget(String key) {
		JSONObject jsonObject = (JSONObject) CrawlCache.jsonObject.get(key);
		String url = jsonObject.getString("url");
		List<QlmEntity> targets = crawlQlmTitle(url);
		List<String> urls = new ArrayList<String>();
		targets.forEach(a->{
			urls.add(a.getUrl());
		});
		crawlContent(urls);
		//过滤
		List<QlmEntity> filterDate = QlmContentFilter.filterDate(QlmTitleCrawl.results);
		List<QlmEntity> filterPhone= QlmContentFilter.filterPhone(filterDate);
		return filterPhone;
	}

	private List<QlmEntity> crawlQlmTitle(String url) {
		QlmTitleCrawl.results.clear();
		QlmTitleCrawl.request(url);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String curDate = sdf.format(Calendar.getInstance().getTime());
		List<QlmEntity> filters =  QlmTitleCrawl.results.stream().filter(a->curDate.equals(a.getUpdateTime())).collect(Collectors.toList());
		QlmTitleCrawl.results.clear();
		QlmTitleCrawl.results.addAll(filters);
		filters.forEach(a->{
			a.setUrl(crawlEntity.getUrl()+"?id="+a.getContentid());
		});
		return filters;
	}

	private void crawlContent(List<String> urls) {
		for (String url:urls){
			QlmContentCrawl qlmContent = new QlmContentCrawl("/qlm/"+System.currentTimeMillis(), false);
			qlmContent.addSeed(url);
			try {
				qlmContent.start(2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				System.out.println("休眠开始："+ new Date(System.currentTimeMillis()));
				//休眠60-180秒钟
				int time = RandomUtil.getRandom() * 1000;
				System.out.println("休眠时间:" + (time / 1000) + "秒");
				Thread.sleep(time);

				System.out.println("休眠结束："+ new Date(System.currentTimeMillis()));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private List<QlmEntity> removeRepeat(List<QlmEntity> newDatas,List<QlmEntity> oldDatas) {
		List<QlmEntity> data = new ArrayList<QlmEntity>();
		if(oldDatas == null || oldDatas.size() <=0) {
			return newDatas;
		}
		for(QlmEntity newData : newDatas) {
			QlmEntity r = getNewData(newData,oldDatas);
			if(r != null) data.add(newData);
		}
		return data;
	}

	private QlmEntity getNewData(QlmEntity newData,List<QlmEntity> oldDatas) {
		for(QlmEntity oldData :oldDatas) {
			if(newData.getContentid().equals(oldData.getContentid())) {
				return null;
			}
		}
		return newData;
	}
}
