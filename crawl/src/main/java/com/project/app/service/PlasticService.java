package com.project.app.service;

import java.util.List;

import com.project.app.entity.NewsEntity;

/**
 * 
* @ClassName: PlasticService 
* @Description: 塑料服务类
* @author luoxiao
* @date 2019年10月17日 下午2:37:23 
*
 */
public interface PlasticService {
	
	/**
	 * 
	* @Title: getUpdatePlastic 
	* @Description: 增量更新
	* @param @param crawlKey
	* @param @return    设定文件 
	* @return List<NewsEntity>    返回类型 
	* @throws
	 */
	public List<NewsEntity> getUpdatePlastic();
	
	/**
	 * 
	* @Title: getAllPlastic 
	* @Description: 爬取全部
	* @param @param crawlKey
	* @param @return    设定文件 
	* @return List<NewsEntity>    返回类型 
	* @throws
	 */
	public List<NewsEntity> getAllPlasticByCrawl();
	
	
	public List<NewsEntity> getAllPlasticByCache();
}
