package com.project.app.service;

import java.util.List;

import com.project.app.entity.NewsEntity;

public interface SteelService {
		
	public List<NewsEntity> getUpateNewPriceByCrawl();
	
	public List<NewsEntity> getAllNewPriceByCrawl();
	
	public List<NewsEntity> getAllNewPriceByCache();
	
	public List<NewsEntity> getUpateNewBaseByCrawl();
	
	public List<NewsEntity> getAllNewBaseByCrawl();
	
	public List<NewsEntity> getAllNewBaseByCache();
	
}
