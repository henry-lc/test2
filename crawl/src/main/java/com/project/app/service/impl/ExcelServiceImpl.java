package com.project.app.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.project.app.cache.FinalDataCache;
import com.project.app.cache.StartFlag;
import com.project.app.constant.CityCache;
import com.project.app.constant.CopperLinkConstant;
import com.project.app.constant.CrawlCache;
import com.project.app.constant.CrawlDeputy;
import com.project.app.constant.DistractCache;
import com.project.app.constant.FilterConstant;
import com.project.app.constant.PlasticCache;
import com.project.app.entity.ExcelEntity;
import com.project.app.service.ExcelService;
import com.project.app.util.ForEachUtils;
import com.project.app.webcollect.collector.DataCache;
import com.project.app.webcollect.excel.CopperExcelContent;
import com.project.app.webcollect.excel.CopperExcelTitle;
import com.project.app.webcollect.excel.ExcelContentCrawl;
import com.project.app.webcollect.excel.ExcelCrawl;
import com.project.app.webcollect.plastic.PlasticContentCrawl;

@Service("excelService")
public class ExcelServiceImpl implements ExcelService{

	@Override
	public List<ExcelEntity> getAllSteelByCrawl() {
		StartFlag.excelSteelIsStart();
		StartFlag.excelSteelStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.STEEL_EXCEL.getCode(),"steel");
		StartFlag.excelSteelStop();
		List<ExcelEntity> lists = DataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.STEEL_EXCEL.getCode());
		CityCache.setExcelEntityProvince(lists);
		List<ExcelEntity> results = handleData(lists,CrawlDeputy.CrawlExcelEnum.STEEL_EXCEL.getCode());
		List<ExcelEntity> sortLists = results.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		return sortLists;
	}

	@Override
	public List<ExcelEntity> getAllAluminumByCrawl() {
		StartFlag.excelAluminumIsStart();
		StartFlag.excelAluminumStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.ALLALUMINUM_EXCEL.getCode(),"aluminum");
		StartFlag.excelAluminumStop();
		List<ExcelEntity> lists = DataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.ALLALUMINUM_EXCEL.getCode());
		CityCache.setExcelEntityProvince(lists);
		List<ExcelEntity> results = handleData(lists,CrawlDeputy.CrawlExcelEnum.ALLALUMINUM_EXCEL.getCode());
		List<ExcelEntity> sortLists = results.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		return sortLists;
	}

	@Override
	public List<ExcelEntity> getAllCopperByCrawl() {
		StartFlag.excelCopperIsStart();
		StartFlag.excelCopperStart();
		//爬取富宝咨询废铜
		crawlTarget(CrawlDeputy.CrawlExcelEnum.COPPER_EXCEL.getCode(),"copper");
		//爬取长江铜业网
		crawlCopperUrl();
		crawlCopperContent();

		StartFlag.excelCopperStop();

		List<ExcelEntity> lists = DataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.COPPER_EXCEL.getCode());
		lists.addAll(CopperExcelContent.lists);
		CityCache.setExcelEntityProvince(lists);
		List<ExcelEntity> results = handleData(lists,CrawlDeputy.CrawlExcelEnum.COPPER_EXCEL.getCode());
		List<ExcelEntity> sortLists = results.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		return sortLists;
	}


	@Override
	public List<ExcelEntity> getAllCellByCrawl() {
		StartFlag.excelCellIsStart();
		StartFlag.excelCellStart();
		crawlTarget(CrawlDeputy.CrawlExcelEnum.CELL_EXCEL.getCode(),"cell");
		StartFlag.excelCellStop();
		List<ExcelEntity> lists = DataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.CELL_EXCEL.getCode());
		CityCache.setExcelEntityProvince(lists);
		List<ExcelEntity> results = handleData(lists,CrawlDeputy.CrawlExcelEnum.CELL_EXCEL.getCode());
		List<ExcelEntity> sortLists = results.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		return sortLists;
	}

	@Override
	public List<ExcelEntity> getAllSteelByCache() {
		List<ExcelEntity> lists = FinalDataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.STEEL_EXCEL.getCode());
		if(lists != null && lists.size() >0) {
			return lists.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		}
		return null;
	}

	@Override
	public List<ExcelEntity> getAllAluminumByCache() {
		List<ExcelEntity> lists = FinalDataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.ALLALUMINUM_EXCEL.getCode());
		if(lists != null && lists.size() >0) {
			return lists.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		}
		return null;
	}


	@Override
	public List<ExcelEntity> getAllCopperByCache() {
		List<ExcelEntity> lists = FinalDataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.COPPER_EXCEL.getCode());
		if(lists != null && lists.size() >0) {
			return lists.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		}
		return null;
	}

	@Override
	public List<ExcelEntity> getAllCellByCache() {
		List<ExcelEntity> lists = FinalDataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.CELL_EXCEL.getCode());
		if(lists != null && lists.size() >0) {
			return lists.stream().sorted(Comparator.comparing(ExcelEntity::getProvince).thenComparing(ExcelEntity::getDistract)).collect(Collectors.toList());
		}
		return null;
	}

	@Override
	public List<ExcelEntity> getAllPlasticByCache(){
		List<ExcelEntity> lists = FinalDataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.PLASTIC_EXCEL.getCode());
		return lists;
	}


	private void crawlTarget(String key,String productName) {
		JSONObject jsonObject = (JSONObject) CrawlCache.jsonObject.get(key);
		String url = jsonObject.getString("url");
		FinalDataCache finalDataCache = FinalDataCache.getInstance();
		DataCache dataCache = DataCache.getInstance();
		if(dataCache.getExcelMaps().get(key) != null) {
			dataCache.getExcelMaps().get(key).clear();
		}
		if(finalDataCache.getExcelMaps().get(key) != null) {
			finalDataCache.getExcelMaps().get(key).clear();
		}
		List<String> urls = crawlExcelUrl(url,productName);
		crawlExcelContent(urls,productName);
	}

	private void crawlPlasticTarget(String key) {
		JSONObject jsonObject = (JSONObject) CrawlCache.jsonObject.get(key);
		String url = jsonObject.getString("url");
		FinalDataCache finalDataCache = FinalDataCache.getInstance();
		DataCache dataCache = DataCache.getInstance();
		if(dataCache.getExcelMaps().get(key) != null) {
			dataCache.getExcelMaps().get(key).clear();
		}
		if(finalDataCache.getExcelMaps().get(key) != null) {
			finalDataCache.getExcelMaps().get(key).clear();
		}
		List<String> lists = new ArrayList<String>();
		lists.add(url);
		crawlExcelContent(lists,"plasticexcel");
	}

	/**
	 * 爬长江铜业网
	 */
	public void crawlCopperUrl(){
		try {
			CopperExcelTitle.urls.clear();
			CopperExcelTitle excelCrawl = new CopperExcelTitle("/excel/coppertwo/"+System.currentTimeMillis(), false);
			excelCrawl.addSeed(Arrays.asList(CopperLinkConstant.links));
			excelCrawl.start(10);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void crawlCopperContent(){
		try {
			CopperExcelContent.lists.clear();
			if(CopperExcelTitle.urls != null && CopperExcelTitle.urls.size()>0) {
				CopperExcelContent excelCrawl = new CopperExcelContent("/excel/coppercontent/"+System.currentTimeMillis(), false);
				excelCrawl.addSeed(CopperExcelTitle.urls);
				excelCrawl.start(10);
			}
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}


	public List<String> crawlExcelUrl(String url,String productName){
		try {
			ExcelCrawl excelCrawl = new ExcelCrawl("/excel/"+productName+"/"+System.currentTimeMillis(),false);
			excelCrawl.addSeed(url);
			excelCrawl.start(2);
			return excelCrawl.getUrls();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void crawlExcelContent(List<String> url,String productName){
		try {
			ExcelContentCrawl excelCrawl = new ExcelContentCrawl("/excelcontent/"+productName+"/"+System.currentTimeMillis(),false);
			excelCrawl.addSeed(url);
			excelCrawl.start(2);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void crawlPlasticExcelContent(String url) {
		try {
			PlasticContentCrawl placticCrawl = new PlasticContentCrawl("/excelcontent/placticCrawl/"+System.currentTimeMillis(),false);
			placticCrawl.addSeed(url);
			placticCrawl.start(2);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	private List<ExcelEntity> handleData(List<ExcelEntity> lists,String key) {
		List<ExcelEntity> result = null;
		if(lists != null) {
			//新增Id
			ForEachUtils.forEach(0, lists, (index,item)->{
				item.setId(String.valueOf(index));
			});
			//分区
			DistractCache.subRegion(lists, null, "getDistract","setRegion");
			//时间排序
			result = lists.stream().sorted(Comparator.comparing(ExcelEntity::getDate).reversed()).collect(Collectors.toList());
			FinalDataCache finalDataCache = FinalDataCache.getInstance();
			finalDataCache.getExcelMaps().put(key, result);
		}
		return result;
	}

	private List<ExcelEntity> handlePlasticData(List<ExcelEntity> lists,String key) {
		List<ExcelEntity> result = null;
		if(lists != null) {
			//新增Id
			ForEachUtils.forEach(0, lists, (index,item)->{
				item.setId(String.valueOf(index));
			});
			//分类
			categroyByProductName(lists);
			categroyByDistract(lists);
			//类型排序
			result = lists.stream().sorted(Comparator.comparing(ExcelEntity::getTax).reversed()).collect(Collectors.toList());
			FinalDataCache finalDataCache = FinalDataCache.getInstance();
			finalDataCache.getExcelMaps().put(key, result);
		}
		return result;
	}

	@Override
	public List<ExcelEntity> getAllPlasticExcelByCrawl() {
		StartFlag.excelPlasticIsStart();
		StartFlag.excelPlasticStart();
		crawlPlasticTarget(CrawlDeputy.CrawlExcelEnum.PLASTIC_EXCEL.getCode());
		StartFlag.excelPlasticStop();
		List<ExcelEntity> lists = DataCache.getInstance().getExcelMaps().get(CrawlDeputy.CrawlExcelEnum.PLASTIC_EXCEL.getCode());
		List<ExcelEntity> results = handlePlasticData(lists,CrawlDeputy.CrawlExcelEnum.PLASTIC_EXCEL.getCode());
		return results;
	}

	private void  categroyByDistract(List<ExcelEntity> lists) {
		if(lists != null && lists.size()>0) {
			for(ExcelEntity a : lists) {
				for(String str : FilterConstant.filterPlaticExcel) {
					if(a.getDistract().indexOf(str) > -1) {
						a.setDistract(a.getDistract().replace(str, ""));
						if(!str.equals("高压") && !str.equals("低压")) {
							a.setTax(str);
						}
						break;
					}
				}
			}
		}
	}

	private void  categroyByProductName(List<ExcelEntity> lists) {
		if(lists != null && lists.size()>0) {
			for(ExcelEntity a : lists) {
				a.setTax(getCategory(a.getProductName(),a.getDistract()));
			}
		}
	}

	private String getCategory(String productName,String distract) {
		if(distract != null) {
			if(distract.indexOf("高压")>-1) {
				return "LDPE";
			}else if(distract.indexOf("低压")>-1) {
				return "HDPE";
			}
		}
		for(Entry<String, List<String>> entry : PlasticCache.map.entrySet()) {
			String key = entry.getKey();
			List<String> value = entry.getValue();
			for(String str : value) {
				if(productName.equals(str)) {
					return key;
				}
			}
		}
		return "其他";
	}

}
