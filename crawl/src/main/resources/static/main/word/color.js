$(function(){
	var localCacheUrl = "/color/word/getColorList";
	var crawlAllDataUrl = "/color/word/getColorCrawl";
	var editUrl = "/color/word/saveColor";
	var exportWordUrl = "/color/word/exportColor";
	var clearCrawlUrl = "/color/word/clearColorCrawlCache";
	
	datagridObj.createDataGrid("colorTable",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl);
	datagridObj.createLoadingMsg("colorTable");
	
	$("#DIV_toolbar").appendTo('.datagrid-toolbar');
})