$(function(){
	
	var localCacheUrl = "/cell/excel/cellExcelListByCache";
	var crawlAllDataUrl = "/cell/excel/cellExcelListByCrawl";
	var editUrl = "/cell/excel/saveCell";
	var exportWordUrl = "/cell/excel/exportCellExcel";
	var clearCrawlUrl = "/cell/excel/clearCellCrawlCache";
	var exchangeUrl = "/cell/excel/exchange";
	
		
	datagridObj.createDataGrid("cellExcel",localCacheUrl,crawlAllDataUrl,editUrl,exportWordUrl,clearCrawlUrl,exchangeUrl);
	datagridObj.createLoadingMsg("cellExcel");
	datagridObj.createPagination("cellExcel",localCacheUrl);
})